#!bin/sh

export AWS_SHARED_CREDENTIALS_FILE=environment/credentials
export ANSIBLE_REMOTE_USER=ec2-user
export ANSIBLE_PRIVATE_KEY_FILE=environment/conchayoro-key
export ANSIBLE_HOST_KEY_CHECKING=False

ansible-galaxy collection install -r environment/requirements.yml
ansible-playbook environment/keys.yml
ansible-playbook environment/provisioning.yml
ansible-playbook environment/install.yml --inventory environment/inventory.aws_ec2.yml